---
title: About Trevor Taubitz
author: ''
---
Trevor Taubitz is an information security professional currently living in Michigan. His interests include

- Golang
- Docker
- Software Engineering
- TDD
- Site Reliability Engineering
- DevOps
- Network Security
- Cloud Security

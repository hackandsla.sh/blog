#!/usr/bin/env bash

# This script identifies all drawio diagrams stored in the "./diagrams" folder,
# and automatically converts them into images using drawio. The resulting files
# are stuck into a subfolder of /static/img.

diagrams_folder="./diagrams"
output_folder="./static/img"

for folder in "${diagrams_folder}"/*; do
  # Extract only the folder name (without "./diagrams")
  name=${folder##*/}
  dest_folder="${output_folder}/${name}"
  mkdir -p "${dest_folder}"

  for diagram in "${folder}"/*; do
    basename=$(basename "${diagram}" .drawio)
    dest_file="${dest_folder}/${basename}.png"
    drawio --export --format png --scale 4 --embed-diagram --output "${dest_file}" "${diagram}"

  done
done